---
pagetitle: "Sponsorship"

listing:
  - id: sponsors
    template: sponsors.ejs
    contents: sponsors.yml
---

::: grid
::: {#sponsors_banner .g-col-12 .title}
# Sponsorship
:::

::: {#sponsors .g-col-12}
:::

::: {#sponsorship .g-col-12 .text_block}
## Sponsorship Prospectus

Here we can link to a prospectus.
:::

<div>

</div>
:::
